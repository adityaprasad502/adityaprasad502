<h1 align='center'>
	<a href="https://adityaprasad.eu.org" rel="nofollow"> <img src="https://media2.giphy.com/media/QssGEmpkyEOhBCb7e1/giphy.gif?cid=ecf05e47a0n14BexZMoP1gqvSbLZSfYigjUvfcXkroScK00bl&rid=giphy.gif" height="36px" width="36px"> </a> Howdy < Peepers />! 
	<a href="https://adityaprasad.eu.org" rel="nofollow"> <img src="https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" height="36px" width="36px"> </a>
	<br>
</h1>

```kt
// try this on a Kotlin IDE, IntelliJ IDEA?
fun cuteMsg() {
    val mt = listOf(65, 68, 73, 84, 89, 65, 32, 80, 82, 65, 83, 65, 68, 32, 83)

    while (true) {
        for (i in 0..13) {
            println("--".repeat(i) + mt[i].toChar())
            Thread.sleep(96)
        }
        for (i in 14 downTo 1) {
            println("--".repeat(i) + mt[i].toChar())
            Thread.sleep(96)
        }
    }
}

fun main() {cuteMsg()}
```

<h2 align='center'> :octocat: Github Stats :electron: </h2>


  <!--START_SECTION:waka-->
<p><a href="https://adityaprasad.eu.org" rel="nofollow"> <img src="https://camo.githubusercontent.com/423cf7a872a9afdd8fc77a95abc11bdac97b25b09ed2e14263b0b19d753a4a9a/687474703a2f2f696d672e736869656c64732e696f2f62616467652f436f646525323054696d6525323073696e636525323032322f30312f323032322d39313425323068727325323032312532306d696e732d626c75653f6c6f676f3d77616b6174696d65" alt="Code Time" data-canonical-src="http://img.shields.io/badge/Code%20Time%20since%2022/01/2022-914%20hrs%2021%20mins-blue?logo=wakatime" style="max-width: 100%;"> </a></p>
<p><strong><g-emoji class="g-emoji" alias="cat" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f431.png">🐱</g-emoji> My GitHub Data</strong></p>
<blockquote>
<p><g-emoji class="g-emoji" alias="trophy" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f3c6.png">🏆</g-emoji> 3,524+ Commits made on Github</p>
<p><g-emoji class="g-emoji" alias="package" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f4e6.png">📦</g-emoji> 23.7 KiB Used in GitHub's Storage</p>
<p><g-emoji class="g-emoji" alias="scroll" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f4dc.png">📜</g-emoji> 4 Public Repositories</p>
<p><g-emoji class="g-emoji" alias="key" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f511.png">🔑</g-emoji> ∞ private repositories.</p>
</blockquote>
<p><g-emoji class="g-emoji" alias="bar_chart" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f4ca.png">📊</g-emoji> <strong>This Week I Spent My Time On</strong></p>
<table>
<tbody><tr><th colspan="4"> <g-emoji class="g-emoji" alias="ghost" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f47b.png">👻</g-emoji> I'm a Night <g-emoji class="g-emoji" alias="owl" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f989.png">🦉</g-emoji></th></tr> 
 <tr>
<td><g-emoji class="g-emoji" alias="sun_with_face" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f31e.png">🌞</g-emoji> Morning</td>
<td>120 commits</td>
<td>▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>24.0%</td>
</tr> 
 <tr>
<td><g-emoji class="g-emoji" alias="city_sunset" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f306.png">🌆</g-emoji> Daytime</td>
<td>129 commits</td>
<td>▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>25.8%</td>
</tr> 
 <tr>
<td><g-emoji class="g-emoji" alias="night_with_stars" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f303.png">🌃</g-emoji> Evening</td>
<td>105 commits</td>
<td>▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>21.0%</td>
</tr> 
 <tr>
<td><g-emoji class="g-emoji" alias="crescent_moon" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f319.png">🌙</g-emoji> Night</td>
<td>146 commits</td>
<td>▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>29.2%</td>
</tr>
</tbody></table>
<table>
<tbody><tr><th colspan="4"><g-emoji class="g-emoji" alias="speech_balloon" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f4ac.png">💬</g-emoji> Programming Languages<g-emoji class="g-emoji" alias="technologist" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f9d1-1f4bb.png">🧑‍💻</g-emoji> </th></tr> 
 <tr>
<td>Kotlin</td>
<td>28 hrs 29 mins</td>
<td>▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>54.46%</td>
</tr> 
 <tr>
<td>XML</td>
<td>19 hrs 3 mins</td>
<td>▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>36.41%</td>
</tr> 
 <tr>
<td>Groovy</td>
<td>58 mins</td>
<td>▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>1.85%</td>
</tr> 
 <tr>
<td>Python</td>
<td>52 mins</td>
<td>▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>1.67%</td>
</tr> 
 <tr>
<td>Docker</td>
<td>50 mins</td>
<td>▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>1.61%</td>
</tr> 
 <tr>
<td>JavaScript</td>
<td>33 mins</td>
<td>▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>1.08%</td>
</tr> 
 <tr>
<td>Gradle</td>
<td>33 mins</td>
<td>▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>1.05%</td>
</tr>
</tbody></table>
<table>
<tbody><tr><th colspan="4"><g-emoji class="g-emoji" alias="fire" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f525.png">🔥</g-emoji> Editors<g-emoji class="g-emoji" alias="fire" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f525.png">🔥</g-emoji> </th></tr> 
 <tr>
<td>Android Studio</td>
<td>49 hrs 26 mins</td>
<td>▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒</td>
<td>94.52%</td>
</tr> 
 <tr>
<td>VS Code</td>
<td>2 hrs 52 mins</td>
<td>▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>5.48%</td>
</tr>
</tbody></table>
<table>
<tbody><tr><th colspan="4"><g-emoji class="g-emoji" alias="computer" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f4bb.png">💻</g-emoji> Operating System<g-emoji class="g-emoji" alias="desktop_computer" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f5a5.png">🖥️</g-emoji> </th></tr> 
 <tr>
<td>Windows</td>
<td>52 hrs 19 mins</td>
<td>▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓</td>
<td>100.0%</td>
</tr>
</tbody></table>
<table>
<tbody><tr><th colspan="4"> <g-emoji class="g-emoji" alias="technologist" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f9d1-1f4bb.png">🧑‍💻</g-emoji> I have more Python Repos <g-emoji class="g-emoji" alias="file_folder" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f4c1.png">📁</g-emoji></th></tr> 
 <tr>
<td>Python</td>
<td>xx repos</td>
<td>▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒</td>
<td>56.52%</td>
</tr> 
 <tr>
<td>JavaScript</td>
<td>x repos</td>
<td>▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>17.39%</td>
</tr> 
 <tr>
<td>Kotlin</td>
<td>x repos</td>
<td>▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>13.04%</td>
</tr> 
 <tr>
<td>HTML</td>
<td>x repos</td>
<td>▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>8.7%</td>
</tr> 
 <tr>
<td>CSS</td>
<td>x repos</td>
<td>▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒</td>
<td>4.35%</td>
</tr>
</tbody></table>
<table>
<tbody><tr><th colspan="4"><g-emoji class="g-emoji" alias="hourglass_flowing_sand" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/23f3.png">⏳</g-emoji> Refresh Stats <g-emoji class="g-emoji" alias="hourglass" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/231b.png">⌛</g-emoji></th></tr>
<tr>
<td>Last Refresh</td>
<td>Sunday</td>
<td>July 16, 2023</td>
<td>19:23:04 IST</td>
</tr>
<tr>
<td>Next Refresh</td>
<td>Monday</td>
<td>July 17, 2023</td>
<td>19:20:00 IST</td>
</tr>
</tbody></table>
<p>
	<a href="https://adityaprasad.eu.org" rel="nofollow">
		<img src="https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif" style="max-width: 100%;">
	</a>
</p>
<details>
<summary><b>Random Jokes for Today</b></summary>
<br>
<pre><code>1 » Without geometry life is pointless.</code></pre>
<pre><code>2 » If at first you don't succeed, sky diving is not for you!</code></pre>
<pre><code>3 » How many apples grow on a tree? All of them!</code></pre>
</details>
<p>
	<a href="https://adityaprasad.eu.org" rel="nofollow">
		<img src="https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif" style="max-width: 100%;">
	</a>
</p>
<details>
<summary><b>Random Facts for Today</b></summary>
<br>
<pre><code>1 » Los Angeles` full name `El Pueblo de Nuestra Senora la Reina de Los Angeles de Porciuncula` is reduced to 3.63% of its size in the abbreviation `L.A.`.</code></pre>
<pre><code>2 » WWII fighter pilots in the South Pacific armed their airplanes while stationed with .50 caliber machine gun ammo belts measuring 27 feet before being loaded into the fuselage. If the pilots fired all their ammo at a target, he went through "the whole 9 yards", hence the term.</code></pre>
<pre><code>3 » Prince Charles is an avid collecter of toilet seats.</code></pre>
</details>
<p>
	<a href="https://adityaprasad.eu.org" rel="nofollow">
		<img src="https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif" style="max-width: 100%;">
	</a>
</p>
<details>
<summary><b>Random Quotes for Today</b></summary>
<br>
<pre><code>1 » A leader is one who knows the way, goes the way, and shows the way. - John C. Maxwell</code></pre>
<pre><code>2 » The hours of folly are measured by the clock; but of wisdom, no clock can measure. - William Blake</code></pre>
<pre><code>3 » Knowledge comes, but wisdom lingers. It may not be difficult to store up in the mind a vast quantity of facts within a comparatively short time, but the ability to form judgments requires the severe discipline of hard work and the tempering heat of experience and maturity. - Calvin Coolidge</code></pre>
</details>

<!--END_SECTION:waka-->



<p>
	<a href="https://adityaprasad.eu.org" rel="nofollow">
		<img src="https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif" style="max-width:100%;">
	</a>
</p>
<details>
	<summary>
		<b>Note For You</b>
	</summary>
	<br>
	<p align="center">
		<i>The GitHub and Wakatime statistics shown here do not capture all of my activity across all platforms and tools. They reflect only a portion of my overall activity on GitHub and time spent using IDEs and editors with Wakatime installed on my personal computer. Keep in mind that my involvement in similar activities elsewhere may not be accounted for in these statistics..!</i>
	</p>
</details>
<p>
	<a href="https://adityaprasad.eu.org" rel="nofollow">
		<img src="https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif" style="max-width:100%;">
	</a>
</p>
<p align="center"> Copyright © 2020 - 2023 <br>
	<br>
	<a href="https://adityaprasad.eu.org" rel="nofollow">
		<img src="https://da.gd/track" style="max-width:100%;">
	</a>
</p>
